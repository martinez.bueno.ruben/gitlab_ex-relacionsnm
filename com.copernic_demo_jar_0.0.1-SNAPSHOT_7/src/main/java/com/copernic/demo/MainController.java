/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo;

import com.copernic.demo.dao.PersonDAO;
import com.copernic.demo.domain.Person;
import com.copernic.demo.domain.Rol;
import com.copernic.demo.services.PersonService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ruben
 */
@Controller
@Slf4j
public class MainController {
    
    @Autowired
    private PersonService PersonService;
    
    //GetMapping para la pantalla de Update para usuarios
    @GetMapping("/person")
    public String formulario(){
        return "person";
    }
    
    @PostMapping("/person")
    public String datosPersona(Person person,Model model){
        PersonService.savePerson(person);
        model.addAttribute("person", person);
        return "resultat";
    }
    //GetMapping para la pantalla de Creacion para un nuevo usuario
    @GetMapping("/person_1")
    public String formulario2(){
        return "person_1";
    }
    
    @PostMapping("/person_1")
    public String datosPersona2(Person person,Model model){
        PersonService.savePerson(person);
        model.addAttribute("person", person);
        return "resultat";
    }
    //Pantalla del CRUD
    @GetMapping("/lista")
    public String lista(Model model){
        List<Person> persons = PersonService.getAllPersons();
        model.addAttribute("persons",persons);
        return "lista";
        
    }
    
    @GetMapping("/update/{id}")
    public String update(Person person,Model model){
        person = PersonService.findPerson(person);
        model.addAttribute("person",person);
        return "person";
    }
    @GetMapping("/delete/{id}")
    public String confirmacion(@PathVariable long id,Model model){
        Person person = PersonService.getPersonById(id);
        model.addAttribute("person",person);
        return "delete";
        
    }
    @GetMapping("/deleteOK/{id}")
    public String deletePerson(Person person){
        PersonService.deletePerson(person.getId());
        return  "redirect:/lista";
    }
    
    //Pantalla para mostrar en la tabla solo los usuarios que contengan @gmail.com
    @GetMapping("/emails")
    public String Emails(Model model){
        List<Person> emails = PersonService.getEmails();
        model.addAttribute("persons",emails);
        return "/lista";
    }
    
}
