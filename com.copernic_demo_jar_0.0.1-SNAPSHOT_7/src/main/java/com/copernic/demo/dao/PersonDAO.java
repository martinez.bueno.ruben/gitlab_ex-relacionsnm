/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.copernic.demo.dao;

import com.copernic.demo.domain.Person;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Ruben
 */
public interface PersonDAO extends JpaRepository<Person, Long>  {
    
    @Query("SELECT p FROM Person p WHERE p.email LIKE '%@gmail.com'")
    List<Person> findEmails();
}
