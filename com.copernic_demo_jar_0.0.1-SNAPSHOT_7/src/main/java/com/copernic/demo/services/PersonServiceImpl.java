/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.demo.services;

import com.copernic.demo.dao.PersonDAO;
import com.copernic.demo.domain.Person;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ruben
 */
@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    private PersonDAO personDao;
    
    @Override
    public Person savePerson(Person person) {
        return personDao.save(person);
    }

    @Override
    public List<Person> getAllPersons() {
        return personDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Person findPerson(Person person) {
      return personDao.findById(person.getId()).orElse(null);
    }

    
    @Override
    @Transactional(readOnly = true)
    public Person getPersonById(Long id) {
        return personDao.findById(id).orElse(null);

    }

    @Override
    @Transactional
    public void deletePerson(Long id) {
        Person person = personDao.findById(id).orElse(null);
        personDao.delete(person);
    
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Person> getEmails(){
        return personDao.findEmails();
    }

    
}
