/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.copernic.demo.services;

import com.copernic.demo.domain.Person;
import java.util.List;

/**
 *
 * @author Ruben
 */
public interface PersonService {
    Person savePerson(Person person);
    public List<Person> getAllPersons();
    public Person findPerson(Person person);
    public Person getPersonById(Long id);
    public void deletePerson(Long id);
    public List<Person> getEmails();
}
